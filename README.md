# Publish On Your Own Site, Syndicate Everywhere
This is a tool I use to deploy an activity feed on my personal website: [jorisgutjahr.eu](https://jorisgutjahr.eu)

## Installation

```
$ git clone https://gitlab.com/CSDUMMI/posse-feed.git
$ cd posse-feed
$ poetry install
``` 

## Usage

```
$ poetry run python -m posse --help
usage: posse-feed [-h] {fetch,make,add,modify,rm,ls,init} ...

POSSE Program using SQLite

positional arguments:
  {fetch,make,add,modify,rm,ls,init}
    fetch               Fetch all input sources and add entries to DB
    make                Compile the database into an output format of choice
                        [HTML, Markdown, RSS]
    add                 Add an input source to read from
    modify              Modify an existing source's name, category or url
    rm                  Remove an input source and all associated items
    ls                  List all input sources
    init                Initialize the database

options:
  -h, --help            show this help message and exit
```

## Architecture
Instead of creating a separate interfaces for authoring posts, it uses RSS feeds from various services to
generate the feed and to syndicate across different platforms.

### Sources
RSS / ATOM Feeds fetched regulary and parsed and entered into an SQLite Database.
You can add, remove, list and modify sources with the CLI. 

### Output Formats
Once one or more sources has been added and fetched, an activity feed can be formatted using `posse make`.

Currently only RSS and Markdown are support, with Markdown being specialised to work with the Pelican SSG frontmatter.

