from dotenv import load_dotenv
import os
load_dotenv()

DATABASE_PATH = os.environ.get("DATABASE_PATH", "feed.db")
TESTING = os.environ.get("TESTING") == "True"
DATABASE_PATH = ":memory:" if TESTING else DATABASE_PATH

RSS_ID = os.environ.get("RSS_ID")
RSS_TITLE = os.environ.get("RSS_TITLE")
RSS_DESCRIPTION = os.environ.get("RSS_DESCRIPTION")
AUTHOR = os.environ.get("AUTHOR")
RSS_LINK = os.environ.get("RSS_LINK")
RSS_LANGUAGE = os.environ.get("RSS_LANGUAGE")
# Use a format string (with {}) to generate permalinks to items on your own website
RSS_ID_PATTERN = os.environ.get("RSS_ID_PATTERN")

