Title: {{ item.title if item.title else name }}
Author: {{ author }}
Category: {{ item.source.category }} 
Tags: {{ item.source.name }}
Slug: {{ name }}
Activity: True {# Indicates that this is an auto-generated post #}
{% if item.published %}Date: {{ item.published.strftime("%Y-%m-%d %H:%S")  }}{% endif %}
{% if item.updated %}Modified: {{ item.updated.strftime("%Y-%m-%d %H:%S") }}{% endif %}


{{ item.description | clean | safe }} 

See: <a href="{{ item.link }}">{{ item.source.name }}</a>

{# TODO: Proper sanitazing #}
