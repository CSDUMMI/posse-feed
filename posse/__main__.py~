from .models import Source, Item, database
from .feed import fetch_one
from .logger import logger
from .make import make
import argparse
import sys

def init(args):
    database.connection()
    database.create_tables([Source, Item])

def fetch(args):
    sources = Source.select()
    if args.source:
        sources = [sources.where(Source.name == args.name).get_or_none()]
    elif args.category:
        sources = sources.where(Source.category == args.category)

    for source in sources:
        fetch_one(source)

    if sources == []:
        logger.info("Found no sources to match. Did you provide the correct name and category?")
    
    
def add(args):
    if Source.select().where(Source.name == args.name).orwhere(Source.source_url == args.url).exists():
        logger.error(f"A source with name {args.name} or for the url {args.url} already exists. Use modify instead.")
        return
    
    source = Source.create(
        name=args.name,
        source_url=args.url.strip(),
        category=args.category.strip() if args.category else None
    )
    
    if args.fetch:
        logger.info(f"Fetching newly added source {args.name}")
        fetch_one(source)

def modify(args):
    source = Source.get_or_none(Source.name == args.name)

    if source is None:
        logger.error(f"Source {args.name} not found. Consider adding it first.")
        return

    if args.new_name:

        if Source.get_or_none(Source.name == args.new_name):
            logger.error(f"Could not change name of source {args.name} to {args.new_name}. Source {args.new_name} already exists")
            return
        
        source.name = args.new_name

    if args.new_category:
        source.category = args.new_category.strip()

    if args.new_url:
        if Source.get_or_none(Source.source_url == args.new_url):
            logger.error(f"Could not change url of source {source.name}. {args.new_url} is already assigned to another source. Remove or modify that source first.")
            return
        source.source_url = args.new_url.strip()
        logger.info("Run fetch to load items from the new url.")
        
    source.save()
    logger.info(f"Successfully modified {source.name}")

    
def remove(args):
    source = Source.select().join(Item).where(Source.name == args.name).get_or_none()

    if source is None:
        logger.error(f"Could not remove {args.name} source. It does not exist")
        return

    if not args.force:
        confirmed = input(f"Do you really want to delete {args.name} and all {source.items.count()} items from it? [y/N]").lower() == "y"

        if not confirmed:
            logger.info("Aborted")
            return
    logger.info(f"Deleting {source.items.count()} items from {source.name}")
    Item.delete().where(Item.source == source).execute()
    logger.info(f"Deleting {source.name} source")
    source.delete_instance()
    logger.info(f"Deleted {source.name} source")

def list(args):
    for source in Source.select().order_by(Source.id):
        logger.info(f"{source.name}\t{source.category}\t{source.source_url}")
        
def run(argv):
    parser = argparse.ArgumentParser(
        prog='posse-feed',
        description='POSSE Program using SQLite'
    )

    subparsers = parser.add_subparsers()

    fetch_and_enter = subparsers.add_parser('fetch', help="Fetch all input sources and add entries to DB")
    fetch_and_enter.add_argument("--category", "-c", help="Only fetch a specific category")
    fetch_and_enter.add_argument("--source", "-s", help="Only fetch from this source")
    fetch_and_enter.set_defaults(func=fetch)

    make_parser = subparsers.add_parser('make', help='Compile the database into an output format of choice [HTML, Markdown, RSS]')
    make_parser.add_argument("format", choices=["html","markdown", "rss"], help="Output format")
    make_parser.add_argument("--source", "-s", action="append", default=[], help="Names of the sources to include. If none provided, all are included")
    make_parser.add_argument("--category", "-c", action="append", default=[], help="Categories of sources to include. If none provided, all are included")
    make_parser.add_argument("--output", "-o", default="output", help="Where to write the output [MD only]")
    make_parser.add_argument("--prefix", "-p", default="activity-", help="To prepend to the files written out [MD only]")
    make_parser.set_defaults(func=make)
    
    add_parser = subparsers.add_parser('add', help='Add an input source to read from')
    add_parser.add_argument("name", help="Name of the source RSS/ATOM feed")
    add_parser.add_argument("url", help="URL of the feed")
    add_parser.add_argument("--category", "-c", help="Assign this source to a category, i.e. 'microblogs'", default=None)
    add_parser.add_argument("--fetch", action=argparse.BooleanOptionalAction, help="Fetch the feed now", default=True)
    add_parser.set_defaults(func=add)

    modify_parser = subparsers.add_parser('modify', help="Modify an existing source's name, category or url")
    modify_parser.add_argument("name", help="Old name of the source")
    modify_parser.add_argument("--new-name", "-n", help="New name of the source", default=None)
    modify_parser.add_argument("--new-category", "-c", help="New category of the source", default=None)
    modify_parser.add_argument("--new-url", "-url", "-u", help="New url of the source", default=None)
    modify_parser.set_defaults(func=modify)
    
    remove_parser = subparsers.add_parser('rm', help='Remove an input source and all associated items')
    remove_parser.add_argument("name", help="Name of the source to delete")
    remove_parser.add_argument("--force", "-f", action="store_true", help="Omit the confirmation dialog")
    remove_parser.set_defaults(func=remove)
    
    list_parser = subparsers.add_parser('ls', help='List all input sources')
    list_parser.set_defaults(func=list)

    init_parser = subparsers.add_parser('init', help='Initialize the database')
    init_parser.set_defaults(func=init)
    
    args = parser.parse_args(argv)
    args.func(args)

if __name__ == '__main__':
    run(sys.argv[1:])
