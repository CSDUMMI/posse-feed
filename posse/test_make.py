from .models import Source, Item, database
from .conftest import db, gitlab
from .make import make_rss, UrlPattern
from .__main__ import run
import feedparser
from pathlib import Path
import tempfile
import shutil
import pytest

@pytest.fixture
def tempfolder():
    path = tempfile.mkdtemp()
    yield path
    shutil.rmtree(path)
    
def test_make_rss(gitlab):
    sources = Source.select()

    id = "https://example.com/"
    title = "A Feed"
    description = "Infos"
    subtitle = "Something"
    author = "CSDUMMI"
    link = "https://example.com/"
    logo = "https://example.com/icon.png"
    language = "en"

    class Pattern(UrlPattern):
        ID_PATTERN = "https://example.com/i/{}"

    result = make_rss(sources, id, title, description, author, link, logo, subtitle, language, Pattern)

    parsed_feed = feedparser.parse(result)
    feed = parsed_feed.feed
    entries = parsed_feed.entries
    assert feed["title"] == "A Feed"
    
    for entry in entries:
        print(entry)
        assert entry.id.startswith("https://example.com/i/")
        assert entry.author == "CSDUMMI"


def test_make_md(gitlab, tempfolder):
    run(["make", "-o", tempfolder, "-p test-", "markdown"])

    path = Path(tempfolder)

    for entry in path.iterdir():
        assert entry.is_file()
        filename = str(entry).rsplit("/")[-1]
        assert filename.startswith("test-")

        id = int(filename.rsplit("-")[-1].rsplit(".")[0])

        assert Item.get_or_none(Item.id == id) is not None
    
        
        
