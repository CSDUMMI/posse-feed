from .models import Source, Item, database
from .__main__ import run
from .conftest import db, gitlab
import builtins
import config
import pytest
import os

def test_config():
    assert config.TESTING

def test_add():
    run(["add", "-c development", "gitlab", "https://gitlab.com/CSDUMMI.atom"])

    source = Source.select().join(Item).where(Source.name=="gitlab").get()

    assert source.category == "development"
    assert source.source_url == "https://gitlab.com/CSDUMMI.atom"
    assert source.items.count() > 0

def test_multiple_add_does_nothing():
    cmd = ["add", "-c development", "gitlab", "https://gitlab.com/CSDUMMI.atom"]
    run(cmd)
    items_count = Item.select().count()
    run(cmd)
    assert Source.select().count() == 1, list(Source.select())
    assert Item.select().count() == items_count

    
def test_remove(gitlab, monkeypatch):
    with monkeypatch.context() as m:
        monkeypatch.setattr(builtins, "input", lambda x: "y")
        rm_cmd = ["rm", "gitlab"]
        run(rm_cmd)
        assert Source.select().count() == 0
        assert Item.select().count() == 0 

def test_modify(gitlab):
    modify_cmd = ["modify", "-n codeberg", "-c dev", "-u https://codeberg.org/CSDUMMI.rss", "gitlab"]
    run(modify_cmd)
    
    source = Source.select().get_or_none()

    assert source is not None, f"Name not changed {list(Source.select())}" 
    assert source.category == "dev"
    assert source.source_url == "https://codeberg.org/CSDUMMI.rss"

    
def test_fetch():
    assert Source.select().count() == 0
    assert Item.select().count() == 0

    add_cmd = ["add", "--no-fetch", "-c development", "gitlab", "https://gitlab.com/CSDUMMI.atom"]
    run(add_cmd)

    assert Source.select().count() == 1
    assert Item.select().count() == 0, list(Item.select()) 

    fetch_cmd = ["fetch"]
    run(fetch_cmd)

    assert Source.select().count() == 1
    assert Item.select().count() > 0
