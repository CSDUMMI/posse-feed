import feedparser
from .models import Source, Item
from .logger import logger
import time
from datetime import datetime, UTC
import requests
from readabilipy import simple_json_from_html_string 
            
@logger.catch
def fetch_one(source: Source, full_article=True):
    """Fetch a feed from the source url and go through all of it's entries.
    If an entry is new, it's added to the db.
    If an entry already exists, but the updated date is more recent than the updated date stored in the db.

    :param full_article: if true, fetches the link of every article and extracts the important content with readiblipy
    """
    feed = feedparser.parse(source.source_url)
    logger.info(f"Fetched {source.name}")
    for entry in feed.entries:
        item = Item.select().join(Source).where(Item.entry_id == entry.id).where(Item.source == source).get_or_none()
        published = entry.get("published_parsed", None)
        published = datetime.fromtimestamp(time.mktime(published)) if published else None
        
        updated = entry.get("updated_parsed", None)
        updated = datetime.fromtimestamp(time.mktime(updated)) if updated else None

        if updated is not None and published is None:
            published = updated
            updated = None
        if published is None:
            published = datetime.now()

        description = fetch_full_article(entry.link, entry.description) if full_article else entry.description
        
        if item is None:
            Item.create(
                title=entry.get("title", None),
                description=description,
                link=entry.link,
                published=published,
                updated=updated,
                source=source,
                entry_id=entry.id
            )
            logger.info(f"Added {entry.id} with title '{entry.get('title', '<missing>')}' to {source.name}") 
        elif item.updated is None and updated is not None or item.updated < updated:
            item.title = entry.title
            item.description = description
            item.link = entry.link
            item.published = published
            item.updated = updated
            item.save()
            logger.info(f"Updated {entry.id} in {source.name} feed")


def fetch_full_article(link, description):
    response = requests.get(link)
    if response.headers["content-type"] != "text/html":
        return description
    
    article = simple_json_from_html_string(response.text, use_readability=True)
    return article["content"]
