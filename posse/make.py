"""Module for rendering sources as HTML, RSS or Markdown
"""
from jinja2 import Environment, FileSystemLoader, select_autoescape
import rfeed
from pathlib import Path
from markupsafe import Markup
from .models import Source, Item
from typing import Dict, Tuple, List
import config
from .logger import logger
import os
import nh3

def clean(text):
    return nh3.clean(text)

env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=True
)

env.filters["clean"] = clean

item_template = env.get_template("item.md")

class UrlPattern:
    """Class to inherit from for defining how to set
    URLs in `make_rss`.

    ID_PATTERN : Pattern for setting the ID of items. If not provided, the source's ID will be used
    """
    ID_PATTERN = None 


def make(args):
    sources = Source.select().where(~Source.disabled).join(Item)
    if args.source != []:
        sources = sources.where(Source.name << args.source)
    if args.category != []:
        sources = sources.where(Source.category << args.category)
    match args.format:
      case "rss":
         class Pattern(UrlPattern):
             ID_PATTERN = config.RSS_ID_PATTERN
         result = make_rss(sources,
                           config.RSS_ID,
                           config.RSS_TITLE,
                           config.RSS_DESCRIPTION,
                           config.AUTHOR,
                           config.RSS_LINK if config.RSS_LINK else config.RSS_ID,
                           language=config.RSS_LANGUAGE,
                           url_patterns=Pattern)
         print(result)
      case "markdown":
         if not Path(args.output).is_dir():
             os.mkdir(args.output)
         make_markdown(sources, config.AUTHOR, output=Path(args.output.strip()), prefix=args.prefix.strip())
         

def make_markdown(sources, author, output=Path("output"), prefix="activity-"):
    """Generate individual markdown files, compatible with pelican posts in the output folder.
    :param sources:
    :param output: path to the output folder. MUST BE CREATED BEFORE CALLING THIS FUNCTION.
    :param prefix: file prefix to use for the markdown files. The item ID is appended.
    """
    for source in sources:
        if source.disabled:
            continue
        for item in source.items:
            name = f"{prefix}{item.sqid}"
            filename = name + ".md"
            with open(output / filename, "w") as fh:
                logger.info(f"Writing {item.title if item.title else item.id} to {output}/{name}.md from source {source.name}")
                fh.write(item_template.render(item=item, name=name, author=author))
    logger.info(f"Writing to {output} completed")
    
      
def to_category(name):
    """Provides a convenience method to convert a single category name to a dict expected by feedgen,
    but not used by RSS.
    """
    return { "term": name, "scheme": "https://example.com/" + name, "label": name }

def make_rss(sources,
             id: str,
             title: str,
             description: str,
             author: str,
             link: str,
             logo =  None,
             subtitle =  None,
             language = None,
             url_patterns = UrlPattern) -> str:
    """Generate an RSS Feed containing entries from all the sources provided

    :param title: Feed title
    :param description: Feed description
    :param author: Feed and item author
    :param link: Feed link
    """
    items = []
    for source in sources:
        for item in source.items:
            guid = url_patterns.ID_PATTERN.format(item.id) if url_patterns.ID_PATTERN else item.entry_id
            items.append(rfeed.Item(
                title = item.title,
                description = Markup(item.description).striptags(),
                link = item.link,
                guid=rfeed.Guid(guid),
                pubDate = item.published,
                author=author
            ))

    feed = rfeed.Feed(
        title=title,
        link=link,
        description=description,
        language=language,
        items=items)
    return feed.rss()
