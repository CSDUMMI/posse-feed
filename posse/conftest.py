from .models import Source, Item, database
from .__main__ import run
import pytest

@pytest.fixture(autouse=True)
def db():
    run(["init"])
    database.transaction()
    yield
    database.rollback()

    
@pytest.fixture
def gitlab(db):
    cmd = ["add", "-c development", "gitlab", "https://gitlab.com/CSDUMMI.atom"]
    run(cmd)
    source = Source.get(Source.name=="gitlab") 
    yield source
    Item.delete().where(Item.source_id == source.id).execute()
    source.delete_instance()
    

