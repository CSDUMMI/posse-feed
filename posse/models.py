import peewee as pw
import config
from sqids import Sqids

sqids = Sqids()

database = pw.SqliteDatabase(config.DATABASE_PATH)

class BaseModel(pw.Model):
    class Meta:
        database = database

        
class Source(BaseModel):
    """An RSS/ATOM Feed to parse as an input to the db.
    The source name is set by the user and displayed
    in the output format. The feed's own title is thus ignored.
    """
    name = pw.CharField(unique=True)
    category = pw.CharField(null=True)
    source_url = pw.TextField(unique=True)
    disabled = pw.BooleanField(default=False)
    
    def __repr__(self):
        return f"<Source: {self.name}>"
    
class Item(BaseModel):
    """An item parsed from an source
    """
    title = pw.TextField(null=True)
    description = pw.TextField()
    published = pw.DateTimeField(null=True)
    updated = pw.DateTimeField(null=True)
    link = pw.TextField(index=True)
    categories = pw.TextField(default="")
    
    source = pw.ForeignKeyField(Source, backref="items", on_delete="CASCADE", constraint_name="fk_source_item")
    entry_id = pw.TextField(index=True)
    
    def __repr__(self):
        return f"<Item: {self.entry_id}>"

    @property
    def sqid(self):
        return sqids.encode([self.id])
